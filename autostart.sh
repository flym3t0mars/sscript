#! /bin/sh
# this script is made by the A_rose team
# to make the installation of A_rose easier
# this will help install base packages
# as well as setup configuration files
# first time use should be as easy as
# running this script and entering the
# password once
# Author Carlos Gudino
# 22-12-21
# 22-03-17 (edit)

echo "Change keyboard layout to dvorak? [y/n]:"
read choice
clear

# Twice as always nice!
if [ $choice == y ]; then
        localectl set-keymap dvorak
        setxkbmap -layout dvorak
        echo "Changed Keyboard layout to dvorak!"
elif [ $choice == n ]; then
        # I guess you like qwerty
	echo "moving on..."
fi;
sleep 5


# installs zsh
sudo pacman -S zsh --noconfirm

# (remove soon) installing omz
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# we are still in tty
/bin/zsh -i -c "omz theme set agnoster"
# sorry you have to manually exit
# in order to continue

# openssh install
sudo pacman -S openssh --noconfirm;sudo systemctl enable sshd; sudo systemctl start sshd
clear;echo "ssh is enabled"; sleep 4

# basic stuff
sudo pacman -S base-devel xf86-video-intel xorg xorg-xinit nitrogen picom --noconfirm

# AUR
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si --noconfirm
cd $HOME
rm -rf yay-git

# adding packages from AUR
yay -S nerd-fonts-mononoki brave-bin shell-color-scripts ulauncher --noconfirm

# adding rose-repo
cat << EOF | sudo tee -a /etc/pacman.conf

[rose-repo]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/flym3t0mars/\$repo/-/raw/main/\$arch
EOF


sudo pacman -Syyu --noconfirm
# yay -S libxft-bgra-git
clear;echo "sorry next part you need to type: [y]"; sleep 5
# the main ingredients
sudo pacman -S  dwm-rose dwmblocks-rose xdg-user-dirs alsa-utils thunar thunar-volman gvfs lxappearance conky feh mpv flameshot ranger neofetch chafa
# rounded corners compositor
yay -S picom-jonaburg-git
# copying default xinitrc to home directory
cp /etc/X11/xinit/xinitrc $HOME/.xinitrc

# removing last 5 lines
count=5
for i in $(seq $count); do
        sed -i '$ d' .xinitrc
done

cat << EOF >> $HOME/.xinitrc
nitrogen --restore &
picom &
dwmblocks &
conky -c $HOME/.config/conky/.conkyrc
exec dwm
EOF

# configs
cd $HOME; git clone https://www.gitlab.com/flym3t0mars/dotfiles.git; cd dotfiles;cp -r .config $HOME/.
# config for rounded corners
sudo mv .config/picom/picom.conf /etc/xdg/picom.conf

## seasonal ##
# sudo cp christmas /opt/shell-color-scripts/colorscripts/.
## seasonal ##

clear; echo "trying to establish configs";sleep 10 
cd $HOME; source .config/*/*;rm -rf $HOME/dotfiles/


# getting wallpapers
mkdir Pictures; cd Pictures; git clone https://gitlab.com/flym3t0mars/wallpapers.git; cd $HOME

# adding lines to .zshrc

cat << EOF >> $HOME/.zshrc
colorscript random

if [ -f ~/.zsh_aliases ]; then
	. ~/.zsh_aliases
fi

[[ \$(fgconsole 2>/dev/null) == 1 ]] && exec startx --vt1
wal -i \$(awk "NR==2" .config/nitrogen/bg-saved.cfg | sed 's/file=//g')

EOF

# adding aliases
touch .zsh_aliases
cat << EOF >> $HOME/.zsh_aliases
alias clear='clear; colorscript random'
alias cls='clear'
alias ccls='cd ~; cls'
EOF

sudo pacman -S python python-pip python-virtualenv --noconfirm
sudo pip install pillow
sudo pip install pywal

clear; echo "Enjoi!"; sleep 15;exit

######## Seasonal ########
# colorscript -e christmas
# sleep 15;exit
######## Seasonal ########
